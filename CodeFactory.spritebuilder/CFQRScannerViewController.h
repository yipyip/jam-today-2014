//
//  CFQRScannerViewController.h
//  CodeFactory
//
//  Created by Marcel Bloemendaal on 12/06/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBarSDK.h"
#import "CFQRScannerViewDelegate.h"


@interface CFQRScannerViewController : UIViewController<ZBarReaderViewDelegate>
{
    ZBarImageScanner *_scanner;
    ZBarReaderView *_barReaderView;
}


@property id<CFQRScannerViewDelegate> qrScannerViewDelegate;

@end

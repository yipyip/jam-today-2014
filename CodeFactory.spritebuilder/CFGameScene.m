//
//  GameScene.m
//  CodeFactory
//
//  Created by Marcel Bloemendaal on 12/06/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CFGameScene.h"
#import "CFQRScannerViewController.h"


@implementation CFGameScene


//--------------------------------------------------------------------------------
//
# pragma mark - Init & dealloc
//
//--------------------------------------------------------------------------------

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _creatureFactory = [CFCreatureFactory new];
        _commandFactory = [CFCommandFactory new];
        _commandQueue = [NSMutableArray new];
        _exampleCreature = nil;
    }
    return self;
}


//--------------------------------------------------------------------------------
//
# pragma mark - Public methods
//
//--------------------------------------------------------------------------------

//---------------------------------------
//	CCNode lifecycle
//---------------------------------------

- (void)didLoadFromCCB
{
    
}


- (void)handleScanButtonTap:(CCButton*)button
{
    NSLog(@"scan!");
    CFQRScannerViewController *scanViewController = [CFQRScannerViewController new];
    scanViewController.qrScannerViewDelegate = self;
    [[CCDirector sharedDirector] presentViewController:scanViewController animated:NO completion:nil];
}

//---------------------------------------
//	QRScannerViewDelegate
//---------------------------------------

- (void)handleQRScannerViewDismissed
{
    
}

- (void)handleQRCodeFound:(NSString*)code
{
    NSRange prefixRange = [code rangeOfString:QR_CODE_FUNCTION_PREFIX];
    if (prefixRange.location == 0 && prefixRange.length == QR_CODE_FUNCTION_PREFIX.length)
    {
        id<CFCommand> command = [_commandFactory createCommandFromFunctionCode:[code substringFromIndex:prefixRange.length]];
        [self executeCommand:command];
    }
}

- (void)setLevel:(NSUInteger)level
{
    _exampleCreature = (CFCreature*)[CCBReader load:@"Creature/CFCreature"];
    CFBody *body;
    switch (level)
    {
        case 1:
        {
            body = [_creatureFactory createBodyWithCreatureColor:CFCreatureColorRed];
            CFTentacle *tentacle = [_creatureFactory createTentacleWithCreatureColor:CFCreatureColorYellow];
            _creature.body = body;
            [_creature addTentacle:tentacle];
            
            _exampleCreature.scale = 0.5f;
            body = [_creatureFactory createBodyWithCreatureColor:CFCreatureColorRed];
            _exampleCreature.body = body;
            
            tentacle = [_creatureFactory createTentacleWithCreatureColor:CFCreatureColorYellow];
            [_exampleCreature addTentacle:tentacle];
            
            tentacle = [_creatureFactory createTentacleWithCreatureColor:CFCreatureColorRed];
            tentacle.scale = 2.0f;
            [_exampleCreature addTentacle:tentacle];
            
            tentacle = [_creatureFactory createTentacleWithCreatureColor:CFCreatureColorYellow];
            [_exampleCreature addTentacle:tentacle];
            
            tentacle = [_creatureFactory createTentacleWithCreatureColor:CFCreatureColorRed];
            tentacle.scale = 2.0f;
            [_exampleCreature addTentacle:tentacle];
            break;
        }
            
        case 2:
        {
            body = [_creatureFactory createBodyWithCreatureColor:CFCreatureColorRed];
            _creature.body = body;
            CFTentacle *tentacle = [_creatureFactory createTentacleWithCreatureColor:CFCreatureColorBlue];
            [_creature addTentacle:tentacle];
            tentacle = [_creatureFactory createTentacleWithCreatureColor:CFCreatureColorBlue];
            [_creature addTentacle:tentacle];
            
            body = [_creatureFactory createBodyWithCreatureColor:CFCreatureColorRed];
            _exampleCreature.body = body;
            
            tentacle = [_creatureFactory createTentacleWithCreatureColor:CFCreatureColorBlue];
            [_exampleCreature addTentacle:tentacle];
            
            tentacle = [_creatureFactory createTentacleWithCreatureColor:CFCreatureColorBlue];
            [_exampleCreature addTentacle:tentacle];
            
            tentacle = [_creatureFactory createTentacleWithCreatureColor:CFCreatureColorRed];
            tentacle.scale = 2.0f;
            [_exampleCreature addTentacle:tentacle];
            
            break;
        }
            
        default:
            break;
    }
    [_exampleContainer addChild:_exampleCreature];
}


//--------------------------------------------------------------------------------
//
# pragma mark - Non-public methods
//
//--------------------------------------------------------------------------------

- (void)executeCommand:(id<CFCommand>)command
{
    NSString *result;
    result = [command executeWithCreature:_creature andCreatureFactory:_creatureFactory];
    [_commandQueue addObject:command];
    if ([result isEqualToString:@"Success"])
    {
        if ([self checkWin])
        {
            _winPanel.scale = 0.0f;
            [_winPanel runAction:[CCActionScaleTo actionWithDuration:0.5f scale:1.0f]];
            _winPanel.visible = YES;
        }
    }
    else
    {
        _feedback;
    }
}

- (void)handleBackButtonTap:(CCButton*)button
{
    [[CCDirector sharedDirector] popScene];
}

- (BOOL)checkWin
{
    if (_creature.body.creatureColor != _exampleCreature.body.creatureColor || _creature.scale != _exampleCreature.scale)
    {
        return NO;
    }
    if (_creature.tentacles.count != _exampleCreature.tentacles.count)
    {
        return NO;
    }
    
    NSArray *tentacles = _creature.tentacles;
    NSArray *exampleTentacles = _exampleCreature.tentacles;
    NSUInteger numTentacles = tentacles.count;
    for (NSUInteger index = 0; index < numTentacles; index++)
    {
        CFTentacle *tentacle = tentacles[index];
        CFTentacle *exampleTentacle = exampleTentacles[index];
        if (tentacle.creatureColor != exampleTentacle.creatureColor || tentacle.scale != exampleTentacle.scale)
        {
            return NO;
        }
    }
    return YES;
}


@end

//
//  CFSceneFactory.h
//  CodeFactory
//
//  Created by Marcel Bloemendaal on 12/06/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CCNode.h"

@class CFApplicationGlobals;

@interface CFSceneFactory : CCNode
{
    CFApplicationGlobals *_applicationGlobals;
}

// Init & dealloc
- (instancetype)initWithApplicationGlobals:(CFApplicationGlobals*)applicationGlobals;

// Public methods
- (CFScene*)getSceneOfType:(NSString*)type;

@end

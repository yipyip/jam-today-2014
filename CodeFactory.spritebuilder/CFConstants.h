//
//  CFConstants.h
//  CodeFactory
//
//  Created by Marcel Bloemendaal on 13/06/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXTERN NSString * const QR_CODE_FUNCTION_PREFIX;

FOUNDATION_EXTERN NSInteger const MAX_TENTACLES;

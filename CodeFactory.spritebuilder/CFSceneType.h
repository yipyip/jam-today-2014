//
//  CFSceneType.h
//  CodeFactory
//
//  Created by Marcel Bloemendaal on 12/06/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

FOUNDATION_EXTERN NSString * const MAIN_SCENE;
FOUNDATION_EXTERN NSString * const GAME_SCENE;

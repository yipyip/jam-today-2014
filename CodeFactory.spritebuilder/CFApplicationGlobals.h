//
//  CFApplicationGlobals.h
//  CodeFactory
//
//  Created by Marcel Bloemendaal on 12/06/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CFSceneFactory.h"

@interface CFApplicationGlobals : NSObject

@property CFSceneFactory *sceneFactory;

// Init & dealloc

@end

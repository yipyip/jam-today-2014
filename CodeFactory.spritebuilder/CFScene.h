//
//  CFScene.h
//  CodeFactory
//
//  Created by Marcel Bloemendaal on 12/06/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CCNode.h"

@class CFApplicationGlobals;


@interface CFScene : CCScene

@property CFApplicationGlobals *applicationGlobals;

@end

//
//  CFQRScannerViewDelegate.h
//  CodeFactory
//
//  Created by Marcel Bloemendaal on 12/06/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CFQRScannerViewDelegate <NSObject>

- (void)handleQRScannerViewDismissed;
- (void)handleQRCodeFound:(NSString*)code;

@end

//
//  CFSceneFactory.m
//  CodeFactory
//
//  Created by Marcel Bloemendaal on 12/06/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CFSceneFactory.h"
#import "CFMainScene.h"
#import "CFApplicationGlobals.h"


@implementation CFSceneFactory


//--------------------------------------------------------------------------------
//
# pragma mark - Init & dealloc
//
//--------------------------------------------------------------------------------

- (instancetype)initWithApplicationGlobals:(CFApplicationGlobals *)applicationGlobals
{
    self = [super init];
    if (self)
    {
        _applicationGlobals = applicationGlobals;
        _applicationGlobals.sceneFactory = self;
    }
    return self;
}

//--------------------------------------------------------------------------------
//
# pragma mark - Public methods
//
//--------------------------------------------------------------------------------

- (CFScene*)getSceneOfType:(NSString *)type
{
    CFScene *scene = nil;
    
    if ([type isEqualToString:MAIN_SCENE])
    {
        scene = (CFScene*)[CCBReader load:@"CFMainScene"];
    }
    else if ([type isEqualToString:GAME_SCENE])
    {
        scene = (CFScene*)[CCBReader load:@"CFGameScene"];
    }
    scene.applicationGlobals = _applicationGlobals;
    return scene;
}

@end

//
//  LDBarcodeScannerViewController.m
//  dinoexpo
//
//  Created by Tim Pelgrim on 24/12/13.
//  Copyright (c) 2013 YipYip. All rights reserved.
//

#import "CFQRScannerViewController.h"


@implementation CFQRScannerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.title = NSLocalizedString(@"Scan code", @"Barcode Scanner");
        _scanner = [[ZBarImageScanner alloc] init];
        
        // TODO: Set the config of the scanner to improve performance and reduce false positives
        // http://zbar.sourceforge.net/iphone/sdkdoc/ZBarImageScanner.html#ZBarImageScanner
        // ==
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    /*
    UIButton *dismissButton = [UIButton new];
    dismissButton.translatesAutoresizingMaskIntoConstraints = NO;
    [dismissButton setImage:[UIImage imageNamed:@"crossIcon.png"] forState:UIControlStateNormal];
    [dismissButton addTarget:self action:@selector(handleDismiss:) forControlEvents:UIControlEventTouchUpInside];
    //[dismissButton setBackgroundImage:[UIImage imageNamed:@"crossIcon.png"] forState:UIControlStateNormal];
    [dismissButton setContentMode:UIViewContentModeCenter];
    [self.view addSubview:dismissButton];
     */
    
//    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[dismissButton(40)]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(dismissButton)]];
//    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[dismissButton(40)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(dismissButton)]];
    
    _barReaderView = [[ZBarReaderView alloc] initWithImageScanner:_scanner];
    _barReaderView.translatesAutoresizingMaskIntoConstraints = NO;
    _barReaderView.readerDelegate = self;
    _barReaderView.torchMode = 0;
    [_barReaderView willRotateToInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation] duration:0];
    [self.view addSubview:_barReaderView];
    
    NSDictionary *vd = @{@"scanner":_barReaderView};
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[scanner]|" options:0 metrics:nil views:vd]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[scanner]|" options:0 metrics:nil views:vd]];
    
    [_barReaderView start];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//==============================================
#pragma mark - UI Events
//==============================================

-(void)handleDismiss:(id)button
{
    
}

//==============================================
#pragma mark - Barcode reader delegate
//==============================================

-(void)readerView:(ZBarReaderView *)readerView didReadSymbols:(ZBarSymbolSet *)symbols fromImage:(UIImage *)image
{
    if (symbols.count == 1)
    {
        for (ZBarSymbol *symbol in symbols)
        {
            [_barReaderView stop];
            [_qrScannerViewDelegate handleQRCodeFound:symbol.data];
            [self dismissViewControllerAnimated:NO completion:nil];
        }
    }
//    for (ZBarSymbol *symbol in symbols)
//    {
//        /*
//        NSError *regexError = nil;
//        NSRegularExpressionOptions options = 0;
//        NSString *pattern = @"^http://www.livingdinosaurs.nl/app/qr\\?dino=\\d+$";
//        NSRegularExpression *expression = [NSRegularExpression regularExpressionWithPattern:pattern options:options error:&regexError];
//        
//        //  count matches
//        NSMatchingOptions matchingOptions = 0;
//        NSRange range = NSMakeRange(0,[symbol.data length]);
//        NSUInteger matchCount = [expression numberOfMatchesInString:symbol.data options:matchingOptions range:range];
//        */
//        NSLog(@"Code read: %@", symbol.data);
//        if (symbols.count == 1)
//        {
//            
//        }
//        else
//        {
//            [_barReaderView stop];
//            
//            // TODO: handle no code found
//        }
//    }
}


-(void)alertViewDismissedWithButtonIndex:(NSInteger)index
{
    [_barReaderView start];
}

@end

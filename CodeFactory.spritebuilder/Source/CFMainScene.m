//
//  MainScene.m
//  PROJECTNAME
//
//  Created by Viktor on 10/10/13.
//  Copyright (c) 2013 Apportable. All rights reserved.
//

#import "CFMainScene.h"
#import "CFGameScene.h"
#import "CFApplicationGlobals.h"


@implementation CFMainScene


//--------------------------------------------------------------------------------
//
# pragma mark - Event handlers
//
//--------------------------------------------------------------------------------

- (void)handleLevel1ButtonTap:(CCButton*)button
{
    CFGameScene *gameScene = (CFGameScene*)[self.applicationGlobals.sceneFactory getSceneOfType:GAME_SCENE];
    
    [gameScene setLevel:1];
    [[CCDirector sharedDirector] pushScene:gameScene];
}

- (void)handleLevel2ButtonTap:(CCButton*)button
{
    CFGameScene *gameScene = (CFGameScene*)[self.applicationGlobals.sceneFactory getSceneOfType:GAME_SCENE];
    
    [gameScene setLevel:2];
    [[CCDirector sharedDirector] pushScene:gameScene];
}

- (void)handleLevel3ButtonTap:(CCButton*)button
{
    CFGameScene *gameScene = (CFGameScene*)[self.applicationGlobals.sceneFactory getSceneOfType:GAME_SCENE];
    
    [gameScene setLevel:3];
    [[CCDirector sharedDirector] pushScene:gameScene];
}

- (void)handleLevel4ButtonTap:(CCButton*)button
{
    CFGameScene *gameScene = (CFGameScene*)[self.applicationGlobals.sceneFactory getSceneOfType:GAME_SCENE];
    
    [gameScene setLevel:4];
    [[CCDirector sharedDirector] pushScene:gameScene];
}

- (void)handleLevel5ButtonTap:(CCButton*)button
{
    CFGameScene *gameScene = (CFGameScene*)[self.applicationGlobals.sceneFactory getSceneOfType:GAME_SCENE];
    
    [gameScene setLevel:5];
    [[CCDirector sharedDirector] pushScene:gameScene];
}


@end

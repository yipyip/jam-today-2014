//
//  CFCreature.h
//  CodeFactory
//
//  Created by Marcel Bloemendaal on 12/06/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CCNode.h"
#import "CFTentacle.h"
#import "CFBody.h"


@interface CFCreature : CCNode
{
    CCNode *_bodyContainer;
    CFBody *_body;
    NSMutableArray *_tentacles;
}


@property CFBody *body;
@property (readonly) NSArray *tentacles;


// Public methods
- (CFTentacle*)addTentacle:(CFTentacle*)tentacle;
- (void)removeTentacle:(CFTentacle*)tentacle;

@end

//
//  CFBody.m
//  CodeFactory
//
//  Created by Marcel Bloemendaal on 12/06/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CFBody.h"

@implementation CFBody

- (void)initCreatureColor:(CFCreatureColor)creatureColor
{
    _creatureColor = creatureColor;
}

@end

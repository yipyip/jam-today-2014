//
//  CFTentacle.h
//  CodeFactory
//
//  Created by Marcel Bloemendaal on 12/06/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CCNode.h"

@interface CFTentacle : CCNode

@property (readonly)CFCreatureColor creatureColor;
@property BOOL extended;

- (void)initCreatureColor:(CFCreatureColor)creatureColor;

@end

//
//  CFCreatureFactory.m
//  CodeFactory
//
//  Created by Marcel Bloemendaal on 12/06/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CFCreatureFactory.h"

@implementation CFCreatureFactory


//--------------------------------------------------------------------------------
//
# pragma mark - Public methods
//
//--------------------------------------------------------------------------------

- (CFBody *)createBodyWithCreatureColor:(CFCreatureColor)creatureColor
{
    CFBody *body = nil;
    
    switch (creatureColor)
    {
        case CFCreatureColorBlue:
            body = (CFBody*)[CCBReader load:@"Creature/CFBlueBody"];
            break;
            
        case CFCreatureColorRed:
            body = (CFBody*)[CCBReader load:@"Creature/CFRedBody"];
            break;
            
        case CFCreatureColorYellow:
            body = (CFBody*)[CCBReader load:@"Creature/CFYellowBody"];
            break;
            
        default:
            break;
    }
    [body initCreatureColor:creatureColor];
    
    return body;
}

- (CFTentacle *)createTentacleWithCreatureColor:(CFCreatureColor)creatureColor
{
    CFTentacle *tentacle = nil;
    
    switch (creatureColor)
    {
        case CFCreatureColorBlue:
            tentacle = (CFTentacle*)[CCBReader load:@"Creature/CFBlueTentacle"];
            break;
            
        case CFCreatureColorRed:
            tentacle = (CFTentacle*)[CCBReader load:@"Creature/CFRedTentacle"];
            break;
            
        case CFCreatureColorYellow:
            tentacle = (CFTentacle*)[CCBReader load:@"Creature/CFYellowTentacle"];
            break;
            
        default:
            break;
    }
    [tentacle initCreatureColor:creatureColor];
    
    return tentacle;
}

@end

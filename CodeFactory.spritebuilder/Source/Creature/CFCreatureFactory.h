//
//  CFCreatureFactory.h
//  CodeFactory
//
//  Created by Marcel Bloemendaal on 12/06/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CFBody.h"
#import "CFTentacle.h"

@interface CFCreatureFactory : NSObject


// Public methods
- (CFBody*)createBodyWithCreatureColor:(CFCreatureColor)creatureColor;
- (CFTentacle*)createTentacleWithCreatureColor:(CFCreatureColor)creatureColor;

@end

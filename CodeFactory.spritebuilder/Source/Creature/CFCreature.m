//
//  CFCreature.m
//  CodeFactory
//
//  Created by Marcel Bloemendaal on 12/06/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CFCreature.h"

@implementation CFCreature

CGFloat const TENTACLE_ADD_RADIUS = 50.0f;


//--------------------------------------------------------------------------------
//
# pragma mark - Init & dealloc
//
//--------------------------------------------------------------------------------

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _body = nil;
        _tentacles = [NSMutableArray new];
    }
    return self;
}


//--------------------------------------------------------------------------------
//
# pragma mark - Properties
//
//--------------------------------------------------------------------------------

- (CFBody *)body
{
    return _body;
}

- (void)setBody:(CFBody *)body
{
    // Clean up current body if any
    if (_body)
    {
        [_bodyContainer removeChild:_body];
    }
    
    _body = body;
    [_bodyContainer addChild:_body];
}

- (NSArray *)tentacles
{
    return _tentacles.copy;
}


//--------------------------------------------------------------------------------
//
# pragma mark - Public methods
//
//--------------------------------------------------------------------------------

//---------------------------------------
//	CCNode lifecycle
//---------------------------------------

- (void)didLoadFromCCB
{
    _bodyContainer.zOrder = 1;
}

//---------------------------------------
//	Tentacles
//---------------------------------------

- (CFTentacle*)addTentacle:(CFTentacle *)tentacle
{
    [self adjustTentacle:tentacle forIndex:_tentacles.count];
    CGFloat angle = -((CGFloat)_tentacles.count / MAX_TENTACLES) * M_PI * 2;
    tentacle.position = ccp(cosf(angle) * TENTACLE_ADD_RADIUS, sinf(angle) * TENTACLE_ADD_RADIUS);
    tentacle.rotation = (-180 / M_PI) * angle;
    [self addChild:tentacle];
    [_tentacles addObject:tentacle];
    return tentacle;
}

- (void)removeTentacle:(CFTentacle*)tentacle
{
    // Remove tentacle
    NSUInteger index = [_tentacles indexOfObject:tentacle];
    if (index != NSNotFound)
    {
        if (_tentacles.count > index)
        {
            [self removeChild:tentacle];
            [_tentacles removeObjectAtIndex:index];
        }
        
        // Move all tentacles beyond it forward
        NSUInteger numTentacles = _tentacles.count;
        for (NSUInteger tentacleIndex = index; tentacleIndex < numTentacles; tentacleIndex++)
        {
            [self adjustTentacle:_tentacles[tentacleIndex] forIndex:tentacleIndex];
        }
    }
}


//--------------------------------------------------------------------------------
//
# pragma mark - Non-public methods
//
//--------------------------------------------------------------------------------

- (void)adjustTentacle:(CFTentacle*)tentacle forIndex:(NSUInteger)index
{
    CGFloat angle = -((CGFloat)index / MAX_TENTACLES) * M_PI * 2;
    tentacle.position = ccp(cosf(angle) * TENTACLE_ADD_RADIUS, sinf(angle) * TENTACLE_ADD_RADIUS);
    tentacle.rotation = (-180 / M_PI) * angle;
}


@end

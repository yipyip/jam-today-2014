//
//  CFTentacle.m
//  CodeFactory
//
//  Created by Marcel Bloemendaal on 12/06/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CFTentacle.h"

@implementation CFTentacle

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _extended = YES;
    }
    return self;
}

- (void)initCreatureColor:(CFCreatureColor)creatureColor
{
    _creatureColor = creatureColor;
}

@end

//
//  CFAddCommand.m
//  CodeFactory
//
//  Created by Marcel Bloemendaal on 12/06/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CFAddTentacleCommand.h"

@implementation CFAddTentacleCommand


//--------------------------------------------------------------------------------
//
# pragma mark - Public methods
//
//--------------------------------------------------------------------------------

- (NSString*)executeWithCreature:(CFCreature *)creature andCreatureFactory:(CFCreatureFactory *)creatureFactory
{
    if (creature.tentacles.count < MAX_TENTACLES)
    {
        CFTentacle *tentacle = [creatureFactory createTentacleWithCreatureColor:creature.body.creatureColor];
        tentacle.scale = 1.0f / creature.scale;
        [creature addTentacle:tentacle];
        return @"Success";
    }
    else
    {
        return @"Maximum number of tentacles exceeded";
    }
}

@end

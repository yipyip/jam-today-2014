//
//  CFEnlargeCommand.m
//  CodeFactory
//
//  Created by Marcel Bloemendaal on 13/06/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CFEnlargeCommand.h"

@implementation CFEnlargeCommand

- (NSString*)executeWithCreature:(CFCreature *)creature andCreatureFactory:(CFCreatureFactory *)creatureFactory
{
    if (creature.scale < 1.5f)
    {
        creature.scale += 0.5f;
        return @"Success";
    }
    else
    {
        return @"Already at maximum size";
    }
}

@end

//
//  CFDoubleTentaclesCommand.m
//  CodeFactory
//
//  Created by Marcel Bloemendaal on 13/06/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CFDoubleTentaclesCommand.h"

@implementation CFDoubleTentaclesCommand


//--------------------------------------------------------------------------------
//
# pragma mark - Public methods
//
//--------------------------------------------------------------------------------

- (NSString*)executeWithCreature:(CFCreature *)creature andCreatureFactory:(CFCreatureFactory *)creatureFactory
{
    NSUInteger numExtendedTentacles = 0;
    for (CFTentacle *tentacle in creature.tentacles)
    {
        if (tentacle.extended)
        {
            numExtendedTentacles++;
        }
    }
    
    if (creature.tentacles.count + numExtendedTentacles <= MAX_TENTACLES)
    {
        for (CFTentacle *tentacle in creature.tentacles)
        {
            if (tentacle.extended)
            {
                CFTentacle *newTentacle = [creatureFactory createTentacleWithCreatureColor:tentacle.creatureColor];
                newTentacle.scale = tentacle.scale;
                [creature addTentacle:newTentacle];
            }
        }
        return @"Success";
    }
    else
    {
        return @"Maximum number of tentacles exceeded";
    }
}

@end

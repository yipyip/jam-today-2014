//
//  CFCommandType.h
//  CodeFactory
//
//  Created by Marcel Bloemendaal on 12/06/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

FOUNDATION_EXTERN NSString * const ADD_TENTACLE_COMMAND;
FOUNDATION_EXTERN NSString * const SUBTRACT_TENTACLE_COMMAND;
FOUNDATION_EXTERN NSString * const DOUBLE_TENTACLES_COMMAND;
FOUNDATION_EXTERN NSString * const HALVE_TENTACLES_COMMAND;
FOUNDATION_EXTERN NSString * const ENLARGE_COMMAND;
FOUNDATION_EXTERN NSString * const SHRINK_COMMAND;

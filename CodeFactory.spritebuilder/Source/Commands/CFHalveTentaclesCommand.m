//
//  CFHalveTentaclesCommand.m
//  CodeFactory
//
//  Created by Marcel Bloemendaal on 13/06/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CFHalveTentaclesCommand.h"

@implementation CFHalveTentaclesCommand

- (NSString*)executeWithCreature:(CFCreature *)creature andCreatureFactory:(CFCreatureFactory *)creatureFactory
{
    NSArray *tentacles = creature.tentacles;
    NSUInteger numTentacles = tentacles.count;
    if (numTentacles % 2 == 0)
    {
        for (NSUInteger index = numTentacles / 2; index < numTentacles; index++)
        {
            CFTentacle *tentacle = tentacles[index];
            [creature removeTentacle:tentacle];
        }
        return @"Success";
    }
    else
    {
        return @"Uneven numbers can't be halved";
    }
}

@end

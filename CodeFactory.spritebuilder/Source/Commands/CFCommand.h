//
//  CFCommand.h
//  CodeFactory
//
//  Created by Marcel Bloemendaal on 12/06/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CFCreature.h"
#import "CFCreatureFactory.h"

@protocol CFCommand <NSObject>

- (NSString*)executeWithCreature:(CFCreature*)creature andCreatureFactory:(CFCreatureFactory*)creatureFactory;

@end

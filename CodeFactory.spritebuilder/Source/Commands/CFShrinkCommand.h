//
//  CFShrinkCommand.h
//  CodeFactory
//
//  Created by Marcel Bloemendaal on 13/06/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CFCommandBase.h"

@interface CFShrinkCommand : CFCommandBase

@end

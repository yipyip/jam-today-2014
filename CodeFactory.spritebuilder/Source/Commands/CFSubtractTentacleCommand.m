//
//  CFSubtractTentacleCommand.m
//  CodeFactory
//
//  Created by Marcel Bloemendaal on 13/06/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CFSubtractTentacleCommand.h"

@implementation CFSubtractTentacleCommand

- (NSString*)executeWithCreature:(CFCreature *)creature andCreatureFactory:(CFCreatureFactory *)creatureFactory
{
    for (CFTentacle *tentacle in [creature.tentacles reverseObjectEnumerator])
    {
        if (tentacle.extended)
        {
            [creature removeTentacle:tentacle];
            return @"Success";
        }
    }
    return @"No tentacles to subtract";
}

@end

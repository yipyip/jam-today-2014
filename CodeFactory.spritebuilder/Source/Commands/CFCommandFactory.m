//
//  CFCommandFactory.m
//  CodeFactory
//
//  Created by Marcel Bloemendaal on 12/06/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CFCommandFactory.h"
#import "CFCommandType.h"

#import "CFAddTentacleCommand.h"
#import "CFSubtractTentacleCommand.h"
#import "CFDoubleTentaclesCommand.h"
#import "CFHalveTentaclesCommand.h"
#import "CFEnlargeCommand.h"
#import "CFShrinkCommand.h"


@implementation CFCommandFactory


//--------------------------------------------------------------------------------
//
# pragma mark - Public methods
//
//--------------------------------------------------------------------------------

- (id<CFCommand>)createCommandFromFunctionCode:(NSString *)functionCode
{
    // Extract function and parameter part from string
    NSRange questionMarkRange = [functionCode rangeOfString:@"?"];
    NSString *functionPart;
    NSString *parameter = nil;
    if (questionMarkRange.location == NSNotFound)
    {
        functionPart = functionCode;
    }
    else
    {
        functionPart = [functionCode substringToIndex:questionMarkRange.location];
        parameter = [functionCode substringFromIndex:questionMarkRange.location + questionMarkRange.length];
    }
    
    // Create matching command
    id<CFCommand> command = nil;
    if ([functionCode isEqualToString:ADD_TENTACLE_COMMAND])
    {
        command = [CFAddTentacleCommand new];
    }
    else if ([functionCode isEqualToString:SUBTRACT_TENTACLE_COMMAND])
    {
        command = [CFSubtractTentacleCommand new];
    }
    else if ([functionCode isEqualToString:DOUBLE_TENTACLES_COMMAND])
    {
        command = [CFDoubleTentaclesCommand new];
    }
    else if ([functionCode isEqualToString:HALVE_TENTACLES_COMMAND])
    {
        command = [CFHalveTentaclesCommand new];
    }
    else if ([functionCode isEqualToString:ENLARGE_COMMAND])
    {
        command = [CFEnlargeCommand new];
    }
    else if ([functionCode isEqualToString:SHRINK_COMMAND])
    {
        command = [CFShrinkCommand new];
    }
    
    return command;
}

@end

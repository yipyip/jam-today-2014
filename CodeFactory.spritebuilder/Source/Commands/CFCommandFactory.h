//
//  CFCommandFactory.h
//  CodeFactory
//
//  Created by Marcel Bloemendaal on 12/06/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CFCommand.h"


@interface CFCommandFactory : NSObject

// Public methods
- (id<CFCommand>)createCommandFromFunctionCode:(NSString*)functionCode;

@end

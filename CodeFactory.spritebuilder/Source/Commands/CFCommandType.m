//
//  CFCommandType.m
//  CodeFactory
//
//  Created by Marcel Bloemendaal on 12/06/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CFCommandType.h"

NSString * const ADD_TENTACLE_COMMAND = @"addTentacle";
NSString * const SUBTRACT_TENTACLE_COMMAND = @"subtractTentacle";
NSString * const DOUBLE_TENTACLES_COMMAND = @"doubleTentacles";
NSString * const HALVE_TENTACLES_COMMAND = @"halveTentacles";
NSString * const ENLARGE_COMMAND = @"enlarge";
NSString * const SHRINK_COMMAND = @"shrink";

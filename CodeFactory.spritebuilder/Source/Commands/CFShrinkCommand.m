//
//  CFShrinkCommand.m
//  CodeFactory
//
//  Created by Marcel Bloemendaal on 13/06/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CFShrinkCommand.h"

@implementation CFShrinkCommand

- (NSString*)executeWithCreature:(CFCreature *)creature andCreatureFactory:(CFCreatureFactory *)creatureFactory
{
    if (creature.scale > 0.5f)
    {
        creature.scale -= 0.5f;
        return @"Success";
    }
    else
    {
        return @"Already at minimum size";
    }
}

@end

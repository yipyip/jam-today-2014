//
//  CFSceneType.m
//  CodeFactory
//
//  Created by Marcel Bloemendaal on 12/06/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CFSceneType.h"

NSString * const MAIN_SCENE = @"MainScene";
NSString * const GAME_SCENE = @"GameScene";

//
//  CFConstants.m
//  CodeFactory
//
//  Created by Marcel Bloemendaal on 13/06/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CFConstants.h"

NSString * const QR_CODE_FUNCTION_PREFIX = @"oncocodeFunction:";

NSInteger const MAX_TENTACLES = 8;


//
//  GameScene.h
//  CodeFactory
//
//  Created by Marcel Bloemendaal on 12/06/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CFScene.h"
#import "CFCreature.h"
#import "CFCreatureFactory.h"
#import "CFQRScannerViewDelegate.h"
#import "CFCommand.h"
#import "CFCommandFactory.h"


@interface CFGameScene : CFScene<CFQRScannerViewDelegate>
{
    CCNode *_exampleContainer;
    CFCreatureFactory *_creatureFactory;
    CFCreature *_creature;
    CFCreature *_exampleCreature;
    CFCommandFactory *_commandFactory;
    NSMutableArray *_commandQueue;
    CCNode *_winPanel;
    CCNode *_feedback;
}


- (void)setLevel:(NSUInteger)level;

@end
